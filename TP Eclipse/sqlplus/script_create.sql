/*=====================================================*/
/* Nom de la base : BALNEOXX */
/* Nom de SGBD : Oracle */
/*=====================================================*/
DROP TABLE Balneo_Paiement;
DROP TABLE Balneo_Facture;
/*==============================================================*/
/* Table : Facture */
/*==============================================================*/
CREATE TABLE Balneo_Facture (
fact_no NUMBER(4) NOT NULL,
fact_date DATE NOT NULL,
fact_montant NUMBER(5) NOT NULL,
CONSTRAINT pk_FactureJava PRIMARY KEY (fact_no)
);
/*==============================================================*/
/* Table : Paiement */
/*==============================================================*/
CREATE TABLE Balneo_Paiement (
fact_no NUMBER(4) NOT NULL,
paie_no NUMBER(5) NOT NULL,
paie_date DATE NOT NULL,
paie_montant NUMBER(6) NOT NULL,
CONSTRAINT pk_paie PRIMARY KEY (fact_no,paie_no) ,
CONSTRAINT fk_paie_fac FOREIGN KEY (fact_no) REFERENCES Balneo_Facture(fact_no)
);
/*==============================================================*/
/* Table : Facture */
/* fact_no NUMBER(4), fact_date DATE, fact_montant NUMBER(5) */
/*==============================================================*/
insert into Balneo_Facture values (1201, TO_DATE('12/07/2012','dd/mm/yyyy'), 1000);
insert into Balneo_Facture values (1202, TO_DATE('22/07/2012','dd/mm/yyyy'), 500);
insert into Balneo_Facture values (1203, TO_DATE('22/08/2012','dd/mm/yyyy'), 257);
insert into Balneo_Facture values (1204, TO_DATE('26/08/2012','dd/mm/yyyy'), 2200);
/*==============================================================*/
/* Table : Paiement */
/* fact_no NUMBER(4),paie_no NUMBER(5) , paie_date DATE, paie_montant NUMBER(6) */
/*==============================================================*/
insert into Balneo_Paiement values (1201, 1, TO_DATE('31/07/2012','dd/mm/yyyy'), 500);
insert into Balneo_Paiement values (1201, 2, TO_DATE('31/08/2012','dd/mm/yyyy'), 500);
insert into Balneo_Paiement values (1203, 1, TO_DATE('31/08/2012','dd/mm/yyyy'), 500);
insert into Balneo_Paiement values (1204, 1, TO_DATE('31/08/2012','dd/mm/yyyy'), 1000);

commit;
